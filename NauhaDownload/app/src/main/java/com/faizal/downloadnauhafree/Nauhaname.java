package com.faizal.downloadnauhafree;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.jorgecastilloprz.FABProgressCircle;
import com.github.jorgecastilloprz.listeners.FABProgressListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class Nauhaname extends AppCompatActivity  {
    ArrayList<TreeMap<String, String>> list = new ArrayList<TreeMap<String, String>>();
    customadapter adapter = new customadapter(list, Nauhaname.this);
    private static int SPLASH_TIME_OUT = 3000;
    String fontPath = "fonts/avenir_book.otf";
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private AdView mAdView;

    Map<String, String> nauha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nauhaname);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        final ListView lv = (ListView) findViewById(R.id.listview);
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        new checknetwork().execute();
        Intent i = getIntent();
        final TreeMap<String, Object> years = new TreeMap<String, Object>  ((HashMap<String,Object>) i.getSerializableExtra("nauhahashmap"));
        Iterator myVeryOwnIterator = years.keySet().iterator();
        while (myVeryOwnIterator.hasNext()) {
            String nauhaname = (String) myVeryOwnIterator.next();
            TreeMap<String, String> map = new TreeMap<String, String>();
            map.put("nauhaname", nauhaname);
            map.put("nauhaurl", years.get(nauhaname).toString());
            list.add(map);
            //  list.add(nauhaname);
            //Toast.makeText(ctx, "Key: "+key+" Value: "+value, Toast.LENGTH_LONG).show();
        }
        lv.setAdapter(adapter);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    class checknetwork extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... params) {
            String result = "";
            if(isInternetAvailable() == false){
                result="N";
            }
            else{
                result="Y";
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if( s.equals("N")){
                showdialog(Nauhaname.this,"Please Check internet Connection and Try again","No Internet Connection");
            }
        }
    }
    public void showdialog(Context context, String dialogmsg, String dialogtitle){
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(dialogtitle);

        // Setting Dialog Message
        alertDialog.setMessage(dialogmsg);

        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.logo);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                System.exit(0);
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    public boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }

    }


    class customadapter extends BaseAdapter implements ListAdapter {


        public ArrayList<TreeMap<String, String>> list = new ArrayList<TreeMap<String, String>>();
        Context context;

        public customadapter(ArrayList<TreeMap<String, String>> list, Context context) {
            this.list = list;
            this.context = context;
        }


        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.nauha_list_item, null);
            }
            final FABProgressCircle fabProgressCircle = (FABProgressCircle)view.findViewById(R.id.fabProgressCircle);
            final TextView nauhatitle = (TextView) view.findViewById(R.id.txttitle);
            Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
            nauhatitle.setTypeface(tf);
            final FloatingActionButton download = (FloatingActionButton) view.findViewById(R.id.fab);
            String nauhatitl = list.get(position).get("nauhaname").toString();
            Log.i("nauhatitle",nauhatitl);
            nauhatitle.setText(nauhatitl);
            download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {




                    if (!hasPermissions(context, PERMISSIONS)) {
                        Toast.makeText(Nauhaname.this, "Please Grant Permissions to save file on Device", Toast.LENGTH_SHORT).show();
                        ActivityCompat.requestPermissions(Nauhaname.this, PERMISSIONS, PERMISSION_ALL);
                    } else {
                        fabProgressCircle.show();
                        fabProgressCircle.beginFinalAnimation();
                        fabProgressCircle.hide();
                        String nauhaurl = list.get(position).get("nauhaurl").toString();
                        URL url = null;
                        try {
                            url = new URL(nauhaurl);
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                        String filename = FilenameUtils.getName(url.getPath());
                        new downloadfile(nauhaurl, filename).execute();
                    }
                }
            });

            return view;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void downloadfile(String downloadUrl, String filename) {
        try {
            URL url = new URL(downloadUrl);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();

            c.setRequestMethod("GET");

            c.connect();

            File dir = new File(
                    Environment.getExternalStorageDirectory() + "/"
                            + "Nauha Download/");
            if (!dir.exists()) {
                dir.mkdir();
                Log.e("INFO", "Directory Created.");
            }
            FileOutputStream fos = new FileOutputStream(filename);//Get OutputStream for NewFile Location

            InputStream is = c.getInputStream();//Get InputStream for connection

            byte[] buffer = new byte[1024];//Set buffer type
            int len1 = 0;//init length
            try {
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);//Write new file
                }


                fos.close();
                is.close();
                Log.i("INFO", "Download COMPLETE");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    class downloadfile extends AsyncTask<String, Void, String>  {

        private String downloadurl;
        private String filename;

        public downloadfile(String downloadurl, String filename) {
            this.downloadurl = downloadurl;
            this.filename = filename;
        }

        @Override
        protected void onPreExecute() {
            Toast.makeText(Nauhaname.this, "Download Starting", Toast.LENGTH_SHORT).show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
         /*   try {
                URL url = new URL(downloadurl);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();

                c.setRequestMethod("GET");

                c.connect();

                File dir = new File(
                        Environment.getExternalStorageDirectory() + "/"
                                + "Nauha Download/");
                if (!dir.exists()) {
                    dir.mkdir();
                    Log.e("INFO", "Directory Created.");
                }
                File f = new File(dir,filename);
                FileOutputStream fos = new FileOutputStream(f);//Get OutputStream for NewFile Location

                InputStream is = c.getInputStream();//Get InputStream for connection

                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                try {
                    while ((len1 = is.read(buffer)) != -1) {
                        fos.write(buffer, 0, len1);//Write new file
                    }


                    fos.close();
                    is.close();
                    Log.i("INFO","Download COMPLETE");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }*/

            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(downloadurl));
            request.setTitle(filename);
            // in order for this if to run, you must use the android 3.2 to compile your app
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            }
            File dir = new File(Environment.getExternalStorageDirectory() + "/" + "Nauha Download");
            if (!dir.exists()) {
                dir.mkdir();
                Log.e("INFO", "Directory Created.");
            }
            Uri fileuri = Uri.fromFile(new File(dir.getAbsolutePath()+File.separator+ filename));
            Log.i("FILE",fileuri.toString());
            request.setDestinationUri(fileuri);
            // get download service and enqueue file
            DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }


    }

}

