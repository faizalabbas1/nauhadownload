package com.faizal.downloadnauhafree;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.jorgecastilloprz.FABProgressCircle;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.util.ArrayList;
import java.util.TreeMap;

public class MyDownloads extends AppCompatActivity {

    ArrayList<TreeMap<String, String>> list = new ArrayList<TreeMap<String, String>>();
    String fontPath = "fonts/avenir_book.otf";
    customadapter adapter = new customadapter(list, MyDownloads.this);
    String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
    int PERMISSION_ALL = 1;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_downloads);

        ListView lv = (ListView) findViewById(R.id.listview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (!hasPermissions(MyDownloads.this, PERMISSIONS)) {
            Toast.makeText(MyDownloads.this, "Please Grant Permissions to create files", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(MyDownloads.this, PERMISSIONS, PERMISSION_ALL);
        }


        String path = Environment.getExternalStorageDirectory().toString()+"/Nauha Download/";
        Log.d("Files", "Path: " + path);
        File directory = new File(path);
        if(directory.exists()) {

            File[] files = directory.listFiles();
            Log.d("Files", "Size: " + files.length);
            for (int i = 0; i < files.length; i++) {
                Log.d("Files", "FileName:" + files[i].getName());
                TreeMap<String, String> map = new TreeMap<String, String>();
                map.put("filepath", files[i].getAbsolutePath().toString());
                map.put("filename", files[i].getName().replace("%20", " ").toString());
                list.add(map);
            }
        }

        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);
                String filename = list.get(i).get("filename").toString();
                String filepath = list.get(i).get("filepath").toString();

                File file = new File(filepath);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Uri fileuri = FileProvider.getUriForFile(MyDownloads.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        file);
                intent.setDataAndType(fileuri, "audio/*");
                startActivity(intent);
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(this, MainActivity.class);
        finish();
        startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case android.R.id.home:
                Intent i = new Intent(this, MainActivity.class);
                finish();
                startActivity(i);
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    class customadapter extends BaseAdapter implements ListAdapter {


        public ArrayList<TreeMap<String, String>> list = new ArrayList<TreeMap<String, String>>();
        Context context;

        public customadapter(ArrayList<TreeMap<String, String>> list, Context context) {
            this.list = list;
            this.context = context;
        }


        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.simple_listitem, null);
            }
            final FABProgressCircle fabProgressCircle = (FABProgressCircle)view.findViewById(R.id.fabProgressCircle);
            fabProgressCircle.setVisibility(View.GONE);
            final TextView nauhatitle = (TextView) view.findViewById(R.id.txttitle);
            Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
            nauhatitle.setTypeface(tf);
            final FloatingActionButton download = (FloatingActionButton) view.findViewById(R.id.fab);
            download.setVisibility(View.GONE);
            String filename = list.get(position).get("filename").toString();
            filename.replace("%20"," ");
            nauhatitle.setText(filename);


            return view;
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
