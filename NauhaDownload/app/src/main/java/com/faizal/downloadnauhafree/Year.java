package com.faizal.downloadnauhafree;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.github.jorgecastilloprz.FABProgressCircle;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Year extends AppCompatActivity {

    TreeMap<String, Object> nauha;
    TreeMap<String, Object> nauhalist;
    String nauhaname;
    ArrayList<TreeMap<String, String>> list = new ArrayList<TreeMap<String, String>>();
    String fontPath = "fonts/avenir_book.otf";
    customadapter adapter = new customadapter(list, Year.this);
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_year);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ListView lv = (ListView) findViewById(R.id.listview);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        Intent i = getIntent();
        final TreeMap<String, Object> years = new TreeMap<String, Object>((HashMap<String,Object>)  i.getSerializableExtra("yearhashmap"));
        Iterator myVeryOwnIterator = years.keySet().iterator();
        while (myVeryOwnIterator.hasNext()) {
            nauhaname = (String) myVeryOwnIterator.next();
            nauha =new  TreeMap<String, Object> ((HashMap<String,Object>) years.get(nauhaname));
            TreeMap<String, String> map = new TreeMap<String, String>();
            map.put("nauhayear", nauhaname);
            map.put("nauhalist", years.get(nauhaname).toString());
            list.add(map);
            //Toast.makeText(ctx, "Key: "+key+" Value: "+value, Toast.LENGTH_LONG).show();
        }

        lv.setAdapter(adapter);
        adapter.notifyDataSetChanged();



        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                adapter.notifyDataSetChanged();
                Intent j = new Intent(Year.this, Nauhaname.class);
                Log.i("nauhaname",list.get(i).get("nauhayear").toString());
                nauhaname = list.get(i).get("nauhayear").toString();
                nauhalist = new TreeMap<String, Object>((HashMap<String,Object>) years.get(nauhaname));
                j.putExtra("nauhahashmap", (Serializable) nauhalist);
                startActivity(j);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(this, MainActivity.class);
        finish();
        startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case android.R.id.home:
                Intent i = new Intent(this, MainActivity.class);
                finish();
                startActivity(i);
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    class customadapter extends BaseAdapter implements ListAdapter {


        public ArrayList<TreeMap<String, String>> list = new ArrayList<TreeMap<String, String>>();
        Context context;

        public customadapter(ArrayList<TreeMap<String, String>> list, Context context) {
            this.list = list;
            this.context = context;
        }


        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.simple_listitem, null);
            }
            final FABProgressCircle fabProgressCircle = (FABProgressCircle)view.findViewById(R.id.fabProgressCircle);
            fabProgressCircle.setVisibility(View.GONE);
            final TextView nauhatitle = (TextView) view.findViewById(R.id.txttitle);
            Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
            nauhatitle.setTypeface(tf);
            final FloatingActionButton download = (FloatingActionButton) view.findViewById(R.id.fab);
            download.setVisibility(View.GONE);
            String nauhatitl = list.get(position).get("nauhayear").toString();
            nauhatitle.setText(nauhatitl);


            return view;
        }
    }
}