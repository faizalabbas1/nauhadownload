package com.faizal.downloadnauhafree;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.jorgecastilloprz.FABProgressCircle;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import org.apache.commons.io.FilenameUtils;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class MainActivity extends AppCompatActivity {
    TextView tv;
    TreeMap<String, Object> years;
    TreeMap<String, Object> data;
    String fontPath = "fonts/avenir_book.otf";

    private AdView mAdView;

    ArrayList<TreeMap<String, String>> list = new ArrayList<TreeMap<java.lang.String, java.lang.String>>();
    customadapter adapter = new customadapter(list, MainActivity.this);
    public static String FACEBOOK_URL = "https://www.facebook.com/nauhadownload";
    public static String FACEBOOK_PAGE_ID = "nauhadownload";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ProgressBar pg=(ProgressBar)findViewById(R.id.pgloading);
        mAdView = (AdView) findViewById(R.id.adView);
        final ListView listView = (ListView) findViewById(R.id.lvrecitor);
        listView.setVisibility(View.INVISIBLE);
        pg.setVisibility(View.VISIBLE);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        new checknetwork().execute();

        new DrawerBuilder().withActivity(this).build();
        //if you want to update the items at a later time it is recommended to keep it in a variable
        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(1).withName("Nauha");
        final SecondaryDrawerItem item2 = new SecondaryDrawerItem().withIdentifier(2).withName("My Downloads");
        final SecondaryDrawerItem item3 = new SecondaryDrawerItem().withIdentifier(3).withName("Nauha Lyrics");
        final SecondaryDrawerItem item4 = new SecondaryDrawerItem().withIdentifier(4).withName("DMCA");
        final SecondaryDrawerItem item5 = new SecondaryDrawerItem().withIdentifier(5).withName("About Us");
        final SecondaryDrawerItem item6 = new SecondaryDrawerItem().withIdentifier(6).withName("Like on Facebook");
        final SecondaryDrawerItem item7 = new SecondaryDrawerItem().withIdentifier(7).withName("Follow on Instagram");
        final SecondaryDrawerItem item8 = new SecondaryDrawerItem().withIdentifier(8).withName("Contact Us");

//create the drawer and remember the `Drawer` result object


        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withTranslucentStatusBar(true)
                .withHeaderBackground(R.drawable.dark_bck)
                .withSelectionListEnabledForSingleProfile(false)
                .addProfiles(
                        new ProfileDrawerItem().withName("Nauha Download").withEmail("Welcome User")
                                .withIcon(getResources()
                                        .getDrawable(R.drawable.logo))
                )
                .build();

        Drawer result = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(headerResult)
                .withToolbar(toolbar)
                .addDrawerItems(
                        item1,
                        //new DividerDrawerItem(),
                        item2,
                        item3,
                        item4,
                        item5,
                        item6,
                        item7,
                        item8
                        // new SecondaryDrawerItem().withName("My Downloads")a
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        if(drawerItem.equals(item2)){
                            Intent i = new Intent(MainActivity.this,MyDownloads.class);
                            finish();
                            startActivity(i);

                        }

                        if(drawerItem.equals(item4)){
                            Intent i = new Intent(MainActivity.this,Webviewclass.class);
                            i.putExtra("url","http://www.nauhadownload.com/dmca/");
                            finish();
                            startActivity(i);

                        }
                        if(drawerItem.equals(item3)){
                            final String appPackageName = "com.faizal.nauhalyricsoffline"; // getPackageName() from Context or Activity object
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }

                        }
                        if(drawerItem.equals(item5)){
                            Intent i = new Intent(MainActivity.this,Webviewclass.class);
                            i.putExtra("url","http://www.nauhadownload.com/about-us/");
                            finish();
                            startActivity(i);

                        }
                        if(drawerItem.equals(item6)){
                            Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
                            String facebookUrl = getFacebookPageURL(MainActivity.this);
                            facebookIntent.setData(Uri.parse(facebookUrl));
                            startActivity(facebookIntent);
                        }
                        if(drawerItem.equals(item7)){
                            Uri uri = Uri.parse("https://www.instagram.com/nauhadownload/");
                            Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

                            likeIng.setPackage("com.instagram.android");

                            try {
                                startActivity(likeIng);
                            } catch (ActivityNotFoundException e) {
                                startActivity(new Intent(Intent.ACTION_VIEW,
                                        Uri.parse("https://www.instagram.com/nauhadownload/")));
                            }
                        }

                        if(drawerItem.equals(item8)){
                            Intent i = new Intent(MainActivity.this,Webviewclass.class);
                            i.putExtra("url","http://www.nauhadownload.com/contact-us/");
                            finish();
                            startActivity(i);
                        }


                        return false;
                    }
                })
                .build();
        result.setSelection(item1);

//Now create your drawer and pass the AccountHeader.Result
     /*  result= new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(headerResult)
        //additional Drawer setup as shown above

    .build();*/




        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("Reciter");


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                ValueEventListener postListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get Post object and use the values to update the UI
                        for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                            data = new TreeMap<String, Object>((HashMap<String,Object>)dataSnapshot.getValue());
                            Iterator myVeryOwnIterator = data.keySet().iterator();
                            while (myVeryOwnIterator.hasNext()) {
                                String recitorname = (String) myVeryOwnIterator.next();
                                TreeMap<String, String> map = new TreeMap<String, String>();
                                map.put("recname", recitorname);
                                years = new TreeMap<String, Object> ((HashMap<String,Object>)data.get(recitorname));
                                list.add(map);
                                //Toast.makeText(ctx, "Key: "+key+" Value: "+value, Toast.LENGTH_LONG).show();
                            }
                        }

                        HashSet hs = new HashSet();
                        hs.addAll(list);
                        list.clear();
                        list.addAll(hs);
                        if (adapter != null) {
                            adapter.notifyDataSetChanged();
                        }
                        pg.setVisibility(View.INVISIBLE);
                        listView.setVisibility(View.VISIBLE);




                        // ...
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Getting Post failed, log a message
                        Log.w("ERROR", "loadPost:onCancelled", databaseError.toException());
                        // ...
                    }
                };
                myRef.addValueEventListener(postListener);
            }
        }, 3000);
        listView.setAdapter(adapter);


        adapter.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                adapter.notifyDataSetChanged();
                String recitorname = list.get(i).get("recname").toString();
                years = new TreeMap<String, Object>((HashMap<String,Object>) data.get(recitorname));
                Intent j = new Intent(MainActivity.this, Year.class);
                j.putExtra("yearhashmap", (Serializable) years);
                finish();
                startActivity(j);
            }
        });

    }



    class checknetwork extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... params) {
            String result = null;
            if(isInternetAvailable() == false){
               result="N";
            }
            else{
                result="Y";
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if( s.equals("N")){
                showdialog(MainActivity.this,"Please Check internet Connection and Try again","No Internet Connection");
            }
        }
    }

    public boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }


    //method to get the right URL to use in the intent
    public String getFacebookPageURL(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
            } else { //older versions of fb app
                return "fb://page/" + FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL; //normal web url
        }
    }


    public void showdialog(Context context, String dialogmsg, String dialogtitle){
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(dialogtitle);

        // Setting Dialog Message
        alertDialog.setMessage(dialogmsg);

        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.logo);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                System.exit(0);
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    class customadapter extends BaseAdapter implements ListAdapter {


        public ArrayList<TreeMap<String, String>> list = new ArrayList<TreeMap<String, String>>();
        Context context;

        public customadapter(ArrayList<TreeMap<String, String>> list, Context context) {
            this.list = list;
            this.context = context;
        }


        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.simple_listitem, null);
            }
            final FABProgressCircle fabProgressCircle = (FABProgressCircle)view.findViewById(R.id.fabProgressCircle);
            fabProgressCircle.setVisibility(View.GONE);
            final TextView nauhatitle = (TextView) view.findViewById(R.id.txttitle);
            Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
            nauhatitle.setTypeface(tf);
            final FloatingActionButton download = (FloatingActionButton) view.findViewById(R.id.fab);
            download.setVisibility(View.GONE);
            String nauhatitl = list.get(position).get("recname").toString();
            nauhatitle.setText(nauhatitl);


            return view;
        }
    }
}
